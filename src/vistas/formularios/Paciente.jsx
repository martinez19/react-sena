import { Encabezado } from "../encabezado/Encabezado";
import './estilos/paciente-estilos.css'
import './estilos/input-estilo.css'


export function Paciente(params) {
    
    return(
        <>
        <Encabezado/>
         <form>
            <label htmlFor="">
                Nombre
                <input type="text" />
            </label>
             <label htmlFor="">
                Apellido
                <input type="text" className="input-estilo" />
            </label>
             <label htmlFor="">
                Diagnostico
                <textarea className="input-estilo"></textarea>
            </label>
        </form> 
        
        </>
    )
}