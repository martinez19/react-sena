import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { Paciente } from './vistas/formularios/Paciente'
import { Prueba } from './vistas/formularios/Prueba'


ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <BrowserRouter>
    <Routes>
      <Route path='/' element={<App/>} />
      <Route path='paciente' element={<Paciente/>}/>
      <Route path='prueba' element={<Prueba/>}/>
    </Routes>
    </BrowserRouter>
  </React.StrictMode>,
)
